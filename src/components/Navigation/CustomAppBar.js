import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {Link} from "react-router-dom";
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
}));
const CustomAppBar = () => {
    const classes=useStyles();
    return (
        <React.Fragment>
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <Button component={Link} to={'/books'} color="inherit">Books</Button>
                        <Button component={Link} to={'/categories'} color="inherit">Categories</Button>
                    </Toolbar>
                </AppBar>
            </div>
        </React.Fragment>
    )
};

export default CustomAppBar;
import React, {useEffect, useState} from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import axios from "axios";


const EditDialog = (props) => {
    const [bookCategory, setBookCategory] = useState("");
    const [categories, setCategories] = useState(null);
    const [bookName, setBookName] = useState("");
    const [authorId, setAuthorId] = useState("");
    const [availableCopies, setCopies] = useState("");

    const handleEdit = (getBooks) => {
        const bookDto = {
            name: bookName,
            category: bookCategory,
            availableCopies: availableCopies,
            authorId: authorId
        };
        debugger;
        axios.put('http://localhost:8080/api/books/' + props.selectedBook.id, bookDto)
            .then(response => {
                getBooks();
            })
            .catch(error => {

            });
        props.handleClose();
    };

    useEffect(() => {
        axios.get('http://localhost:8080/api/books/categories')
            .then(response => {
                setCategories(response.data);
            })
            .catch(error => {
                //TODO
            });
        if (props.selectedBook){
            setBookName(props.selectedBook.name);
            setCopies(props.selectedBook.availableCopies);
            setBookCategory(props.selectedBook.category);
            setAuthorId(props.selectedBook.authorId);
        }
    }, [props]);
    return (
        <React.Fragment>
            <Dialog open={props.open} onClose={props.handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Edit Book</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Book name"
                        type="text"
                        fullWidth
                        value={bookName}
                        onChange={(event) => setBookName(event.target.value)}
                    />
                    <FormControl fullWidth={true}>
                        <InputLabel id="demo-simple-select-label">Book Category</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={bookCategory}
                            onChange={(event) => setBookCategory(event.target.value)}
                        >
                            {categories ? categories.map((category, index) => (
                                <MenuItem key={index} value={category}>
                                    {category}
                                </MenuItem>
                            )) : null};

                        </Select>
                    </FormControl>
                    <TextField
                        margin="dense"
                        id="copies"
                        label="Available copies"
                        type="number"
                        fullWidth
                        value={availableCopies}
                        onChange={(event) => setCopies(event.target.value)}
                    />
                    <TextField
                        margin="dense"
                        id="author"
                        label="Author ID"
                        type="number"
                        value={authorId}
                        onChange={(event) => setAuthorId(event.target.value)}
                        fullWidth
                    />

                </DialogContent>
                <DialogActions>
                    <Button onClick={props.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={() => handleEdit(props.books)} color="primary">
                        Edit
                    </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment>
    );

};

export default EditDialog;
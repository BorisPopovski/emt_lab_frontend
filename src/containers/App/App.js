import logo from '../../logo.svg';
import './App.css';
import React from "react";
import Button from "@material-ui/core/Button";
import {BrowserRouter} from "react-router-dom";
import Layout from "../Layout/Layout";

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
          <Layout/>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;

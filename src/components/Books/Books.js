import React, {useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import makeStyles from "@material-ui/core/styles/makeStyles";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import axios from "axios";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import RemoveIcon from '@material-ui/icons/Remove';
import AddDialog from "../UI/Dialogs/AddDialog";
import EditDialog from "../UI/Dialogs/EditDialog";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

const Books = () => {

    const classes = useStyles();
    const [books, setBooks] = useState(null);
    const [openAddDialog, setOpenAddDialog] = useState(false);
    const [selectedBook, setSelectedBook] = useState({});
    const [openEditDialog, setOpenEditDialog] = useState(false);

    const markAsTaken = (bookId) => {
        axios.patch('http://localhost:8080/api/books/' + bookId + '/taken')
            .then(response => {
                getBooks();
            })
            .catch(error => {

            });
    };
    const getBooks = () => {
        axios.get('http://localhost:8080/api/books')
            .then(response => {
                setBooks(response.data);
            })
            .catch(error => {

            });
    };
    const deleteBook = (bookId) => {
        axios.delete('http://localhost:8080/api/books/' + bookId)
            .then(response => {
                getBooks();
            })
            .catch(error => {

            });
    };

    const removeBook = () => {
        setSelectedBook(null);
    };

    const editBook = (book) => {
        setSelectedBook(book);
        setOpenEditDialog(true);
    };

    useEffect(() => {
        getBooks();
    }, []);


    return (
        <React.Fragment>
            <Button color={"primary"} variant={"outlined"} onClick={() => setOpenAddDialog(true)}>Add new book</Button>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell>Book Name</TableCell>
                            <TableCell align="right">Book Category</TableCell>
                            <TableCell align="right">Author Name</TableCell>
                            <TableCell align="right">Author Country</TableCell>
                            <TableCell align="right">Available Copies</TableCell>
                            <TableCell align="right"/>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {books ? books.map((book) => (
                            <TableRow key={book.id}>
                                <TableCell component="th" scope="row">
                                    {book.name}
                                </TableCell>
                                <TableCell align="right">{book.category}</TableCell>
                                <TableCell
                                    align="right">{book.author.authorName + " " + book.author.authorSurname}</TableCell>
                                <TableCell align="right">{book.author.authorCountry.countryName}</TableCell>
                                <TableCell align="right">{book.availableCopies}</TableCell>
                                <TableCell align="right">
                                    <IconButton aria-label="delete" onClick={() => deleteBook(book.id)}>
                                        <DeleteIcon/>
                                    </IconButton>
                                    <IconButton aria-label="edit" onClick={() => editBook(book)}>
                                        <EditIcon/>
                                    </IconButton>
                                    <IconButton aria-label="decrease" onClick={() => markAsTaken(book.id)}>
                                        <RemoveIcon/>
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        )) : null}
                    </TableBody>
                </Table>
            </TableContainer>

            <EditDialog open={openEditDialog}
                        selectedBook={selectedBook}
                        removeSelectedBook={removeBook}
                        books={() => getBooks()}
                        handleClose={() => setOpenEditDialog(false)}/>

            <AddDialog open={openAddDialog}
                       books={() => getBooks()}
                       handleClose={() => setOpenAddDialog(false)}/>
        </React.Fragment>
    );
};

export default withRouter(Books);
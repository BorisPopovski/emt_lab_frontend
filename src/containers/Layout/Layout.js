import React from "react";
import CustomAppBar from "../../components/Navigation/CustomAppBar";
import {Route, Switch, withRouter} from "react-router-dom";
import Books from "../../components/Books/Books";
import Categories from "../../components/Categories/Categories";

const Layout = () => {
    let routes = (
        <Switch>
            <Route exact path="/">
                <Books/>
            </Route>
            <Route exact path="/books">
                <Books/>
            </Route>
            <Route exact path="/categories">
                <Categories/>
            </Route>
        </Switch>
    );
    return (
        <React.Fragment>
            <CustomAppBar/>
            {routes}
        </React.Fragment>
    );
};

export default withRouter(Layout);
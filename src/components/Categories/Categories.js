import React, {useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import axios from "axios";

const Categories = () => {
    const [categories,setCategories]= useState(null);
    useEffect(()=>{
       axios.get('http://localhost:8080/api/books/categories')
           .then(response=>{
                setCategories(response.data);
           })
           .catch(error=>{

           });
    },[]);
    return (
        <React.Fragment>
            <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <Typography variant="h6">
                        Categories
                    </Typography>
                    <div>
                        <List>
                            {categories ? categories.map((category, index) => (
                                    <ListItem key={index}>
                                        <ListItemText>
                                            {category}
                                        </ListItemText>
                                    </ListItem>
                                )
                            ):null}
                        </List>
                    </div>
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default withRouter(Categories);